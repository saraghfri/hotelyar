// use pinia technology as a state management
import {defineStore} from "pinia";

export const useGlobalStore = defineStore('global', {
    state: () => {
        return {
            displayToast: false,
            full_loading: false,
        }
    },
    actions: {
        setFullLoading(value) {
            this.full_loading = value
        },
        toastTimeOut() {
            this.displayToast = true

            setTimeout(() => {
                this.displayToast = false
            }, 3000);
        },
        logout() {
            const router = useRouter()
            const route = useRoute()
            // if local storage is clear, there is nothing to expire. so we just redirect user to login
            if (!JSON.parse(localStorage.getItem('userInfo'))) {
                router.push({
                    path: `/`
                })
            } else {
                this.expireToken()
            }
        },
        expireToken() {
            const router = useRouter()
            const route = useRoute()
            this.full_loading = true

            useNuxtApp().$api.get(`/auth/logout`)
                .then(response => {
                    if (response.data.success) {
                        router.push({
                            path: `/`
                        })
                    }
                }).catch(error => {
                //
                console.log(error, ' error')
                this.full_loading = false

            })
        },
    },
})
